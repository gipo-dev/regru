<?php


namespace App\Services;


use Facebook\WebDriver\Chrome\ChromeDriver;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use NunoMaduro\LaravelConsoleDusk\Drivers\Chrome;

class ProxyChrome extends Chrome
{
    /**
     * @var string
     */
    private $proxy;

    public function __construct($proxy)
    {
        $this->proxy = $proxy;
    }

    public function getDriver()
    {

        $options = (new ChromeOptions)->addArguments(
            [
                '--disable-gpu',
                '--headless',
            ]
        );

        $capabilities = DesiredCapabilities::chrome()
            ->setCapability(
                ChromeOptions::CAPABILITY,
                $options
            );
        if ($this->proxy !== null)
            $capabilities = $capabilities->setCapability(WebDriverCapabilityType::PROXY, [
                'proxyType' => 'manual',
                'httpProxy' => $this->proxy,
                'sslProxy' => $this->proxy,
            ]);
        return RemoteWebDriver::create('http://localhost:9515', $capabilities);
    }
}
