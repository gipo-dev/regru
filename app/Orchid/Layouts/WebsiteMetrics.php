<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Metric;

class WebsiteMetrics extends Metric
{
    /**
     * Get the labels available for the metric.
     *
     * @return array
     */
    protected $labels = [
        'Статус',
        'Выполнено/Ошибки/Прогресс',
        'Дата запуска',
        'Дата окончания',
        'Сайтов в очереди',
    ];

    /**
     * The name of the key to fetch it from the query.
     *
     * @var string
     */
    protected $target = 'metrics';
}
