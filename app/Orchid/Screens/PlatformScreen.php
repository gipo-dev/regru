<?php

declare(strict_types=1);

namespace App\Orchid\Screens;

use App\Jobs\AddCheckDomainsJob;
use App\Jobs\StartDomainProcessingCommand;
use App\Models\Domain;
use App\Orchid\Layouts\ChartsLayout;
use App\Orchid\Layouts\ChartsRegistratorLayout;
use App\Orchid\Layouts\WebsiteMetrics;
use App\Services\BatchManager;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class PlatformScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Управление доменами';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * @var BatchManager
     */
    private $batchManager;

    /**
     * @var \Illuminate\Bus\Batch
     */
    private $batch;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        $this->batchManager = new BatchManager();
        $this->batch = $this->batchManager->find(BatchManager::TYPE_WEBSITE);

        return [
            'domains' => Domain::latest('id')->paginate(50),
            'chart' => Domain::countForGroup('status_id')->toChart(static function ($title) {
                return __('loops.domain.status.' . $title);
            }),
            'chart_registrator' => Domain::countForGroup('registrator')->toChart(),
            'metrics' => [
                ['keyValue' => __('loops.domain.job.status.' . ($this->batch ? (int)$this->batch->finished() : 0))],
                ['keyValue' => ($this->batch ? ($this->batch->processedJobs() . '/' . $this->batch->failedJobs . '/' . $this->batch->progress()) : 0) . '%'],
                ['keyValue' => ($this->batch ? $this->batch->createdAt->format('H:i d.m.Y') : '')],
                ['keyValue' => ($this->batch && $this->batch->finishedAt ?
                    $this->batch->finishedAt->format('H:i d.m.Y') : '')],
                ['keyValue' => ($this->batch ? $this->batch->pendingJobs : 0)],
            ],
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            DropDown::make('Удалить')
                ->icon('trash')
                ->list([
                    Button::make('Не рег ру')->type(Color::WARNING())
                        ->method('bulkDelete')->parameters([
                            'status_id' => Domain::STATUS_INVALID_REGISTRATOR,
                        ]),
                    Button::make('Отфильтрованные')->type(Color::ERROR())
                        ->method('bulkDelete')->parameters([
                            'status_id' => Domain::STATUS_INVALID,
                        ]),
                ]),

            DropDown::make('Экспорт')
                ->icon('doc')
                ->list([
                    Button::make('Отправленные')->type(Color::SUCCESS())
                        ->method('exportSent')->rawClick(),
                    Button::make('Валидные')->type(Color::PRIMARY())
                        ->method('exportValid')->rawClick(),
                    Button::make('Не рег ру')->type(Color::WARNING())
                        ->method('exportNotRegRu')->rawClick(),
                    Button::make('Отфильтрованные')->type(Color::ERROR())
                        ->method('exportFiltered')->rawClick(),
                ]),

            Button::make('Запустить')->type(Color::SUCCESS())->icon('control-play')
                ->method('start')
                ->hidden($this->batch ? !$this->batch->finished() : false),

            Button::make('Остановить')->type(Color::DANGER())->icon('control-pause')
                ->method('stop')
                ->hidden($this->batch ? $this->batch->finished() : true),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): array
    {
        return [
            WebsiteMetrics::class,

            Layout::columns([
                Layout::rows([
                    TextArea::make('new_domains')->rows(10)
                        ->placeholder('Каждый домен с новой строки'),
                    Button::make('Добавить домены')->method('generate')
                        ->type(Color::SUCCESS())->icon('check'),
                ]),
                ChartsLayout::class,
                ChartsRegistratorLayout::class,
            ]),

            Layout::table('domains', [
                TD::make('domain', 'Домен')->render(function (Domain $domain) {
                    return Link::make($domain->domain)->href('http://' . $domain->domain)->target('_blank');
                }),
                TD::make('status_id', 'Статус')->render(function (Domain $domain) {
                    return __('loops.domain.status.' . $domain->status_id);
                }),
                TD::make('', 'Информация')->render(function (Domain $domain) {
                    return $domain->getInfo();
                }),
                TD::make()
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Domain $domain) {
                        return Group::make([
                            Button::make('Удалить')->icon('trash')->type(Color::DANGER())
                                ->method('delete')
                                ->confirm('Удалить?')
                                ->parameters([
                                    'id' => $domain->id,
                                ]),
                        ]);
                    }),
            ]),
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function exportSent(Request $request)
    {
        return $this->export(
            Domain::where('status_id', Domain::STATUS_SENT)
            , 'sent');
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function exportFiltered(Request $request)
    {
        return $this->export(
            Domain::where('status_id', Domain::STATUS_INVALID)
            , 'filtered');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function exportValid(Request $request)
    {
        return $this->export(
            Domain::where('status_id', Domain::STATUS_VALID)
            , 'valid');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function exportNotRegRu(Request $request)
    {
        return $this->export(
            Domain::where('status_id', Domain::STATUS_INVALID_REGISTRATOR)
            , 'not-reg-ru');
    }

    /**
     * @param $rows
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    private function export($domains, $filename)
    {
        $rows = [];
        $domains->chunk(300, function ($list) use (&$rows) {
            foreach ($list as $domain) {
                $rows[] = [
                    $domain->domain,
                    $domain->data['title'] ?? '',
                    $domain->data['description'] ?? '',
                    $domain->getInfo(),
                ];
            }
        });
        return response()->streamDownload(function () use ($rows, $filename) {
            $csv = fopen('php://output', 'wb');
            fputs($csv, chr(0xEF) . chr(0xBB) . chr(0xBF)); // BOM

            fputcsv($csv, ['Домен', 'Заголовок', 'Описание', 'Информация'], ';');

            collect($rows)->each(function (array $row) use ($csv) {
                fputcsv($csv, $row, ';');
            });

            fclose($csv);

            return $csv;
        }, $filename . '.csv');
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function generate(Request $request)
    {
        $new = 0;
        preg_match_all('/([0-9a-zA-Z\-]+\.[0-9a-zA-Z\-]+)/', $request->new_domains, $domains);
        foreach (array_filter($domains[1]) as $domain) {
            $word = Domain::firstOrCreate(['domain' => trim($domain)]);
            if ($word->wasRecentlyCreated) {
                $new++;
            }
        }
        AddCheckDomainsJob::dispatch();
        Toast::success("Добавлено $new новых доменов");
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function delete(Request $request)
    {
        Domain::where('id', $request->id)->delete();
        Toast::success('Удалено');
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function bulkDelete(Request $request)
    {
        $count = Domain::where('status_id', $request->status_id)->delete();
        Toast::success('Удалено ' . $count . ' доменов');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @throws \Throwable
     */
    public function start(Request $request)
    {
        StartDomainProcessingCommand::dispatch();
        Toast::success('Обработка сайтов запущена');
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function stop(Request $request)
    {
        $manager = (new BatchManager())->find(BatchManager::TYPE_WEBSITE);
        $manager->cancel();
        Toast::warning('Обработка сайтов остановлена');
    }
}
