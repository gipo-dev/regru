<?php


namespace App\Services;


use App\Models\Domain;
use App\Models\StopWord;
use GuzzleHttp\Client;
use Illuminate\Support\Str;

class DomainChecker
{
    public function checkDomain(Domain $domain)
    {
        $content = (new Client([
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36',
            ],
            'timeout' => 30,
        ]))->get($domain->domain)->getBody()->getContents();
        $content = mb_convert_encoding($content, 'UTF-8', 'UTF-8');
        $domain->setData('page', $content);
        $this->setMeta($domain, $content);
        $markers = [
            $this->setCMS($domain, $content),
            $this->checkMinusWords($domain),
        ];
        foreach ($markers as $marker) {
            if (!$marker) {
                $domain->update(['status_id' => Domain::STATUS_INVALID]);
                throw new \Exception('Валидация не пройдена');
            }
        }
        $domain->update(['status_id' => Domain::STATUS_VALID]);
    }

    /**
     * @param \App\Models\Domain $domain
     * @param string $content
     */
    private function setMeta(Domain $domain, string $content)
    {
        preg_match('~<title>(.+)<~U', $content, $title);
        preg_match('~.+meta.+title.+.*content="(.+)"~U', $content, $title_2);
        $domain->setData('title', $title[1] ?? $title_2[1] ?? '?');

        preg_match('~.+meta.+description.+.*content="(.+)"~U', $content, $description);
        $domain->setData('description', $description[1] ?? '?');
    }

    /**
     * @param \App\Models\Domain $domain
     * @param string $content
     * @return bool
     */
    private function setCMS(Domain $domain, string $content)
    {
        $signs = [
            'wp' => [
                'wp-',
                '/wp-includes/',
                '/wp-content/',
//                'wordpress'
            ],
            'dle' => [
//                'DataLife',
                'dle_admin',
                '/engine/classes/',
                'dle-'
            ],
        ];
        $detected_cms = null;
        foreach ($signs as $cms => $group) {
            foreach ($group as $sign) {
                if (str_contains($content, $sign)) {
                    $detected_cms = $cms;
                    break 2;
                }
            }
        }

        $domain->setData('cms', $detected_cms ?? '?');

        return $detected_cms != null;
    }

    /**
     * @param \App\Models\Domain $domain
     * @return bool
     */
    public function checkMinusWords(Domain $domain)
    {
        $domain = $domain->refresh();

        foreach (['title', 'description'] as $key) {
            foreach (StopWord::all()->pluck('word') as $word) {
                if (Str::contains($domain->data[$key] ?? '', $word)) {
                    $domain->setData('minus_word', [
                        'field' => $key,
                        'word' => $word,
                    ]);
                    return false;
                }
            }
        }

        return true;
    }
}
