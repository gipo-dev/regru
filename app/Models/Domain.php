<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

class Domain extends Model
{
    use HasFactory;
    use AsSource;
    use Chartable;

    const STATUS_NEW = 1,
        STATUS_NEED_VERIFICATION = 2,
        STATUS_SENT = 3,
        STATUS_VALID = 4,
        STATUS_INVALID = 5,
        STATUS_INVALID_REGISTRATOR = 6;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var int[]
     */
    protected $attributes = ['status_id' => self::STATUS_NEW];

    /**
     * @var string[]
     */
    protected $casts = ['data' => 'array'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(MailAccount::class);
    }

    /**
     * @param string $key
     * @param $value
     */
    public function setData(string $key, $value)
    {
        $this->update([
            'data' => array_merge($this->data ?? [], [
                $key => $value,
            ])
        ]);
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        $data = [];
        if ($this->data['cms'] ?? false)
            $data[] = 'CMS: ' . $this->data['cms'] ?? '';
        if ($this->status_id == Domain::STATUS_INVALID && ($this->data['minus_word']['word'] ?? false))
            $data[] = 'Совпадение слова "' . ($this->data['minus_word']['word'] ?? '') . '" в поле ' . ($this->data['minus_word']['field'] ?? '');
        if ($this->data['registrator'] ?? false)
            $data[] = 'Регистратор: "' . ($this->data['registrator'] ?? '') . '"';
        if ($this->data['sent_time'] ?? false)
            $data[] = 'Письмо отправлено: ' . ($this->data['sent_time'] ?? '');

        return implode(', ', $data);
    }
}
