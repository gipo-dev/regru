<?php


namespace App\Services;


use Facebook\WebDriver\Chrome\ChromeDriver;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;

class Browser
{
    /**
     * @var \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    private $driver;

    /**
     * Browser constructor.
     */
    public function init()
    {

    }

    /**
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    public function driver()
    {
        return $this->driver;
    }

    /**
     * @param string $url
     */
    public function go(string $url)
    {
        $this->driver()->navigate()->to($url);
    }

    /**
     * @param string $selector
     * @return \Facebook\WebDriver\Remote\RemoteWebElement
     */
    public function findElement(string $selector)
    {
        return $this->driver()->findElement(WebDriverBy::cssSelector($selector));
    }

    /**
     * @param string $name
     */
    public function screenshot($name = 'screenshot')
    {
        $this->driver()->takeScreenshot($name . '.png');
    }
}
