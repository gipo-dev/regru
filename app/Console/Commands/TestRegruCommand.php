<?php

namespace App\Console\Commands;

use App\Services\CaptchaSolver;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Faker\Factory as Faker;

class TestRegruCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:regru';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var \App\Services\CaptchaSolver
     */
    private $captchaSolver;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->reloadProxy();
        $client = $this->getClient();
        $resp = $this->getClient()->get('https://myip.ru/index_small.php')->getBody()->getContents();
        $this->info($resp);

        $this->captchaSolver = new CaptchaSolver();
        $resp = $client->get('https://www.reg.ru/whois/admin_contact')->getBody()->getContents();
        preg_match('/name="joke".+value="(.+)"/', $resp, $joke);
        preg_match('/name="_csrf".+content="(.+)"/', $resp, $csrf);
        $key = json_decode($this->client->get('https://www.reg.ru/misc/get_recaptcha_site_key', [
            'headers' => [
                'x-csrf-token' => $csrf[1],
            ],
        ])->getBody()->getContents(), true);

        $result = $client->post('https://www.reg.ru/whois/admin_contact', [
            'form_params' => [
                'joke' => $joke[1],
                '_csrf' => $csrf[1],
//                'dname' => 'mosmuseum.ru',
                'dname' => 'game-tips.ru',
                'fio' => 'Олег монгол',
                'email' => 'Moertuse@mail.ru',
                'message' => 'Дайте денях',
                'g-recaptcha-response' => $this->captchaSolver->solveCaptcha($resp, 'https://www.reg.ru/whois/admin_contact', $key['site_key']),
            ],
        ])->getBody()->getContents();
        preg_match('~\<div class\="b-message__content"\>(.+)\<\/div\>~', $result, $message);
        dd($message[1]);
        return 0;
    }

    private function getClient()
    {
        return new Client([
            'cookies' => true,
            'proxy' => 'http://om.mobileproxy.space:64017',
            'headers' => [
                'User-Agent' => Faker::create()->userAgent,
                'origin' => 'https://www.reg.ru',
                'referer' => 'https://www.reg.ru/',
            ],
        ]);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function reloadProxy()
    {
        while (true) {
            $resp = json_decode((new Client())->get('https://mobileproxy.space/reload.html?proxy_key=328c56147393e12fa2fe25903ba2804c&format=json', [
                'timeout' => 120,
            ])->getBody()->getContents(), true);
            if ($resp['status'] == 'OK')
                break;
        }
        echo('Прокси презагружен: ' . ($resp['new_ip']) . PHP_EOL);
    }
}
