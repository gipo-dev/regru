<?php


namespace App\Services;


use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;

class BatchManager
{
    const TYPE_WEBSITE = 'parse_websites';

    /**
     * @param array $jobs
     * @param string $name
     * @return \Illuminate\Bus\Batch
     * @throws \Throwable
     */
    public function start(array $jobs, string $name)
    {
        return Bus::batch($jobs)->name($name)
            ->allowFailures()->dispatch();
    }

    /**
     * @param string $name
     * @return \Illuminate\Bus\Batch|null
     */
    public function find(string $name)
    {
        $id = DB::table('job_batches')
                ->where('name', $name)->latest()->first()->id ?? null;
        if ($id)
            return Bus::findBatch($id);
        return null;
    }
}
