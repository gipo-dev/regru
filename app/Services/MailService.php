<?php


namespace App\Services;


use AmaxLab\YandexPddApi\Client;
use App\Models\Domain;
use Ddeboer\Imap\Search\Email\From;
use Ddeboer\Imap\Search\Flag\Unseen;
use Ddeboer\Imap\Search\Text\Body;
use Ddeboer\Imap\Search\Text\Subject;
use Ddeboer\Imap\SearchExpression;
use Ddeboer\Imap\Server;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MailService
{
    const DEFAULT_PASSWORD = '1711--29031xlsa';
    const IMAP_SERVER = 'imap.yandex.ru';

    /**
     * @var \AmaxLab\YandexPddApi\Client
     */
    private $pdd;

    /**
     * @var \Ddeboer\Imap\Server
     */
    private $mail;

    /**
     * @var \Ddeboer\Imap\ConnectionInterface
     */
    private $connection;

    public function __construct()
    {
        $this->pdd = new Client(config('services.yandex_pdd.key'));
        $this->mail = new Server(self::IMAP_SERVER);
    }

    /**
     * @param $login
     * @param string $password
     * @return array
     */
    public function createMail($login, $password = self::DEFAULT_PASSWORD)
    {
        $config = [
            'login' => $login,
            'password' => $password,
        ];
        $this->pdd->getMailBoxManager()->addMailBox(config('services.yandex_pdd.domain'), $login, $password);
        $config['imap_password'] = $this->setConfigurations($config);
        return $config;
    }

    /**
     * @param $config
     * @return \Laravel\Dusk\Browser|null
     */
    public function setConfigurations($config)
    {
        $uid = null;
        $imap_password = null;
        $this->console->browse(function ($browser) use ($config, &$imap_password) {
            /** @var \Laravel\Dusk\Browser $browser */
            try {
                $this->loginInBrowser($browser, $config);

                $imap_password = $this->getImapKey($browser, $config);

                $browser->visit('https://mail.yandex.ru/');
                $browser->waitFor('.svgicon.svgicon-mail--Yandex-header_settings', 30);
                $current_url = $browser->getOriginalBrowser()->driver->getCurrentURL();
                preg_match('/uid=(.*)#/U', $current_url, $uid);
                $uid = $uid[1];
                $browser->visit("https://mail.yandex.ru/?uid=$uid#setup/filters-create");
                $browser->waitFor('.js-filters-condition-add');
                $browser->click('.b-form-layout__button_delete');
                $browser->script("$('.js-setup_passwd + .b-form-layout__line .js-setup-filter-forward-checkbox').click()");

                sleep(1);
                $browser->type('forward_address', config('services.yandex_pdd.forward_address'));
                $browser->screenshot('mail/filter_filled');
                $browser->click('.js-add-button');
                $browser->waitForText('Кроме того, вы можете управлять входящей почтой с помощью чёрного и белого списков.', 10);
                $browser->screenshot('mail/filter_created');

                $browser->quit();
            } catch (\Exception $exception) {
                $browser->quit();
            }
        });

        return $imap_password;
    }

    /**
     * @param $login
     * @param string $password
     */
    public function loginToMail($login, $password = self::DEFAULT_PASSWORD)
    {
        $this->connection = $this->mail->authenticate($login, $password);
    }

    /**
     * @return \Ddeboer\Imap\MessageIteratorInterface
     */
    public function getMessages($text = null)
    {
        $mailbox = $this->connection->getMailbox('Sent');
        $search = new SearchExpression();
//        $search->addCondition(new From('noreply@reg.ru'));
        $search->addCondition(new Unseen());
        if ($text)
            $search->addCondition(new Subject($text));
        return $mailbox->getMessages($search, \SORTDATE, true);
    }

    /**
     * @return bool
     */
    public function confirmSending($domain)
    {
        sleep(5);
        try {
            echo('Получаю сообщения: ' . $domain->domain . PHP_EOL);
            preg_match('/href="(.+send_msg_token.+)"/U', $this->getMessages($domain->domain)->current()->getBodyHtml(), $link);
            $this->getMessages($domain->domain)->current()->markAsSeen();
            $confirm_url = $link[1];
            echo('Перехожу по ссылке: ' . $confirm_url . PHP_EOL);

            $resp = (new \GuzzleHttp\Client())->get($confirm_url)->getBody()->getContents();
            if (Str::contains($resp, 'отправлено администратору'))
                return true;
            else {
                preg_match('/b-message__content">(.+)</', $resp, $message);
                echo 'Текст ошибки: ' . ($message[1] ?? '') . PHP_EOL;
                return false;
            }
        } catch (\Exception $exception) {
            echo('Сообщение не найдено' . PHP_EOL);
        }
        echo('Ошибка: Письмо не найдено' . PHP_EOL);
        return false;
    }

    /**
     * @param $browser
     * @param $config
     * @return \Laravel\Dusk\Browser
     * @throws \Facebook\WebDriver\Exception\TimeOutException
     */
    private function getImapKey(&$browser, $config)
    {
        /** @var \Laravel\Dusk\Browser $browser */
        $browser->visit('https://passport.yandex.ru/profile/');
        $browser->waitForText('Пароли и авторизация', 20);
//        $browser->click('.SectionHead-icon_key');
        $browser->script("$('.SectionHead-icon_key').click();");
        $browser->screenshot('mail/profile');
        $browser->waitForText('Включить пароли приложений', 10);
//        sleep(3);
        $browser->click('[data-t="pssp:passwords_disabled"] span');
        $browser->waitFor('.Button2_view_action');
        $browser->click('.Button2_view_action');
        $browser->waitForText('Создать новый пароль');
        $browser->click('.app-passwords-list_empty .Button2_view_action');
        $browser->waitForText('Создать пароль приложения', 10);
        $browser->click('.item.type-mail');
        $browser->waitForText('Название пароля для доступа к почте');
        $browser->type('app_pwd-device-name', 'API');
        $browser->screenshot('mail/filled');
        $browser->click('.create_app_pwd-button-generate .Button2.Button2_size_l.Button2_view_action');
        $browser->waitForText('Пароль «API» готов:', 20);
        $browser->screenshot('mail/pwd');
        return $browser->text('.create_app_pwd-created-text');
    }

    /**
     * @param $login
     * @param $password
     * @return \Ddeboer\Imap\MessageIteratorInterface
     */
    public function readAllMessages($login, $password)
    {
        $this->loginToMail($login, $password);
        $mailbox = $this->connection->getMailbox('Sent');
        $search = new SearchExpression();
        $search->addCondition(new From('noreply@reg.ru'));
        $search->addCondition(new Unseen());
        $messages = $mailbox->getMessages($search, \SORTDATE, true);

        foreach ($messages as $message) {
            $message->markAsSeen();
        }
    }

    private function loginInBrowser(&$browser, $config)
    {
        /** @var \Laravel\Dusk\Browser $browser */
        $browser->resize(1920, 1080);
        $browser->visit('https://passport.yandex.ru/auth');
        $browser->waitFor('input[name=login]', 10);
        $browser->type('login', $config['login'] . '@' . config('services.yandex_pdd.domain'));
        $browser->click('button[type="submit"]');
        $browser->waitForText('Введите пароль', 10);
        $browser->type('passwd', $config['password']);
//        $browser->screenshot('mail/login');
        $browser->click('button[type="submit"]');
        $browser->screenshot('mail/login');
        $browser->waitForText('Завершить регистрацию');
        $browser->click('.submit-button');
    }
}
