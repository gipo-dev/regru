<?php

namespace App\Console\Commands;

use App\Models\MailAccount;
use App\Services\MailService;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class CreateMailsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:mails {--count=10}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $faker = Faker::create();
        $mail = (new MailService($this));

        for ($i = 0; $i < $this->option('count'); $i++) {
            $username = $faker->userName;
            $account = MailAccount::create([
                'email' => $username . '@' . config('services.yandex_pdd.domain'),
                'password' => MailService::DEFAULT_PASSWORD,
            ]);
            $mail_data = $mail->createMail($username);
            $account->update([
                'imap_password' => $mail_data['imap_password'],
            ]);
        }
    }
}
