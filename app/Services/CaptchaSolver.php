<?php


namespace App\Services;


use Rucaptcha\Client;

class CaptchaSolver
{
    public function __construct()
    {
        $this->client = new Client(config('services.rucaptcha.key'));
    }

    public function solveCaptcha(string $html, string $page_url, string $key = null)
    {
        if (!$key)
            $key = $this->findKey($html);
        $task_id = $this->client->sendRecaptchaV2($key, $page_url);
        echo('Запрос...' . PHP_EOL);
        while (true) {
            sleep(5);
            $result = $this->client->getCaptchaResult($task_id);
            if ($result !== false) {
                echo('Результат получен' . PHP_EOL);
                return $result;
            }
        }
    }

    private function findKey($html)
    {
        preg_match('~www.google.com/recaptcha/api2/anchor.*?k=(.+)&~U', $html, $key);
        return $key[1] ?? false;
    }
}
