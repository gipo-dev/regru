<?php

namespace App\Console\Commands;

use App\Models\Domain;
use App\Services\DomainChecker;
use Illuminate\Console\Command;

class CheckDomainCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'domain:check {domain_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $checker = new DomainChecker();
        if ($this->argument('domain_id'))
            $domains = Domain::where('id', $this->argument('domain_id'))->get();
        else
            $domains = Domain::where('status_id', Domain::STATUS_NEW)->get();
        foreach ($domains as $i => $domain) {
            /** @var Domain $domain */
            try {
                $checker->checkDomain($domain);
                $this->info($i);
            } catch (\Exception $exception) {
                $domain->update(['status_id' => Domain::STATUS_INVALID]);
                $domain->setData('cms', $exception->getMessage());
//                $domain->setData('error', $exception->getMessage());
//                dd($exception);
                $this->error($domain->domain . ': ' . $exception->getMessage());
            }
        }
    }
}
