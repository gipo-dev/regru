<?php

namespace App\Orchid\Screens;

use App\Http\Requests\ProxyRequest;
use App\Models\Proxy;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ProxyScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Прокси';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'proxies' => Proxy::paginate(100),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [

        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                TextArea::make('proxies')->rows(5)
                    ->placeholder("Введите каждое прокси с новой строки\nhttp://ip:port"),
                Button::make('Добавить прокси')->method('generate')
                    ->type(Color::SUCCESS())->icon('check'),
            ]),

            Layout::table('proxies', [
                TD::make('data', 'Подключение'),
                TD::make()
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Proxy $proxy) {
                        return Group::make([
                            Button::make('Удалить')->icon('trash')->type(Color::DANGER())
                                ->method('delete')
                                ->confirm('Удалить?')
                                ->parameters([
                                    'id' => $proxy->id,
                                ]),
                        ]);
                    }),
            ]),
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function delete(Request $request)
    {
        Proxy::where('id', $request->id)->delete();
        Toast::success('Удалено');
    }

    /**
     * @param \App\Http\Requests\ProxyRequest $request
     */
    public function generate(ProxyRequest $request)
    {
        $new = 0;
        foreach (array_filter(explode(PHP_EOL, $request->proxies)) as $proxy) {
            $proxy = Proxy::firstOrCreate(['data' => trim($proxy)]);
            if ($proxy->wasRecentlyCreated)
                $new++;
        }
        Toast::success("Добавлено $new новых прокси");
    }
}
