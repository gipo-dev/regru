<?php

namespace App\Providers;

use App\Services\Browser;
use Illuminate\Support\ServiceProvider;

class BrowserProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Browser::class, function () {
            return new Browser();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
