<?php

namespace App\Console\Commands;

use App\Models\Proxy;
use App\Services\ProxyChrome;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Faker\Factory as Faker;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Chrome\ChromeProcess;
use NunoMaduro\LaravelConsoleDusk\Manager;

class CheckProxyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'proxy:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        for ($i = 0; $i < 10; $i++) {
            $this->reloadProxy();
            $this->info($this->getClient()->get('https://myip.ru/index_small.php')->getBody()->getContents());
//            $browser = new Manager(new ProxyChrome('to.mobileproxy.space:64027'));
//            $browser->browse($this, function ($browser) {
//                /** @var Browser $browser */
//                try {
//                    $browser->visit('https://myip.ru/index_small.php');
//                    $this->info($browser->getOriginalBrowser()->driver->getPageSource());
//                    $browser->quit();
//                    $this->info('Меняю адрес...');
//                    (new Client())->get('https://mobileproxy.space/reload.html?proxy_key=328c56147393e12fa2fe25903ba2804c&format=json', [
//                        'timeout' => 120,
//                        'verify' => false,
//                    ]);
//                    $this->info('Адрес изменен');
//                } catch (\Exception $exception) {
//                    $this->error($exception->getMessage());
//                    if ($browser)
//                        $browser->quit();
//                }
//            });
        }
    }


    private function getClient()
    {
        return new Client([
            'cookies' => true,
            'proxy' => 'http://om.mobileproxy.space:64017',
            'headers' => [
                'User-Agent' => Faker::create()->userAgent,
                'origin' => 'https://www.reg.ru',
                'referer' => 'https://www.reg.ru/',
            ],
        ]);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function reloadProxy()
    {
        while (true) {
            $resp = json_decode((new Client())->get('https://mobileproxy.space/reload.html?proxy_key=328c56147393e12fa2fe25903ba2804c&format=json', [
                'timeout' => 120,
                'verify' => false,
            ])->getBody()->getContents(), true);
            if ($resp['status'] == 'OK')
                break;
            else
                echo json_encode($resp);
        }
        echo('Прокси презагружен: ' . ($resp['new_ip']) . PHP_EOL);
    }
}
