<?php


namespace App\Services;


use App\Models\Domain;
use App\Models\MailAccount;
use Faker\Factory as Faker;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class RegruService
{
    private $links = [
        'index' => 'https://www.reg.ru/whois/admin_contact',
    ];

    /**
     * @var \Illuminate\Console\Command
     */
    private $console;

    /**
     * @var \App\Services\CaptchaSolver
     */
    private $captchaSolver;


    /**
     * RegruService constructor.
     */
    public function __construct(Command $console)
    {
        $this->console = $console;
        $this->captchaSolver = new CaptchaSolver();
    }

    public function sendMessage(MailAccount $account, Domain $domain, $browser, $command, $data)
    {
        $this->reloadProxy();
        $client = $this->getClient();
        $resp = $client->get('https://myip.ru/index_small.php')->getBody()->getContents();
        echo('Proxy: ' . $resp . PHP_EOL);

        $this->captchaSolver = new CaptchaSolver();
        $resp = $client->get('https://www.reg.ru/whois/admin_contact', [
            'timeout' => 180,
        ])->getBody()->getContents();
        preg_match('/name="joke".+value="(.+)"/', $resp, $joke);
        preg_match('/name="_csrf".+content="(.+)"/', $resp, $csrf);
        echo('Получаю токен' . PHP_EOL);
        $key = json_decode($client->get('https://www.reg.ru/misc/get_recaptcha_site_key', [
            'headers' => [
                'x-csrf-token' => $csrf[1],
            ],
            'timeout' => 180,
        ])->getBody()->getContents(), true);
        echo('Отправляю запрос...' . PHP_EOL);
        try {
            $result = $client->post('https://www.reg.ru/whois/admin_contact', [
                'form_params' => [
                    'joke' => $joke[1],
                    '_csrf' => $csrf[1],
                    'dname' => $data['domain'],
                    'fio' => $data['name'],
                    'email' => $data['email'],
                    'message' => $data['message'],
                    'timeout' => 180,
                    'g-recaptcha-response' => $this->captchaSolver->solveCaptcha($resp, 'https://www.reg.ru/whois/admin_contact', $key['site_key']),
                ],
            ]);
            $content = $result->getBody()->getContents();
            echo('Запрос отправлен. Код: ' . $result->getStatusCode() . PHP_EOL);
        } catch (\Exception $exception) {
            echo('Ошибка рег ру: ' . $exception->getMessage() . PHP_EOL);
        }
        preg_match('~\<div class\="b-message__content"\>(.+)\<\/div\>~', $content, $message);
        $message = $message[1] ?? '';
        if (!Str::contains($message, 'ссылке, отправленной на')) {
            if (!Str::contains($content, 'id="content"')) {
                $domain->update(['status_id' => Domain::STATUS_INVALID_REGISTRATOR]);
                $message = 'Неверный регистратор';
            }
            Storage::put('page_content.html', $content);
            echo('Ошибка отправки письма: ' . $message . PHP_EOL);
            throw new \Exception('Ошибка отправки письма: ' . $message);
        }
        $domain->update(['mail_account_id' => $account->id]);
        echo('Сообщение:' . $message[1] . PHP_EOL);
    }

    /**
     * @param \App\Models\MailAccount $account
     * @param \App\Models\Domain $domain
     * @param $data
     * @throws \Facebook\WebDriver\Exception\TimeOutException
     */
    public function _old_sendMessage(MailAccount $account, Domain $domain, $browser, $command, $data)
    {

        $browser->browse($command, function ($browser) use ($domain, $account, $data) {
            /** @var \Laravel\Dusk\Browser $browser */
            try {
                $browser->resize(1920, 1080);

                $browser->visit('https://myip.ru/index_small.php');
                echo($browser->getOriginalBrowser()->driver->getPageSource() . PHP_EOL);
                $browser->visit($this->links['index']);
                echo('Вводим данные...' . PHP_EOL);
                $browser->waitFor('#g-recaptcha', 30);

                $source = $browser->getOriginalBrowser()->driver->getPageSource();
                $captcha_solve = $this->captchaSolver->solveCaptcha($source, $this->links['index']);
                $this->fillInputs($browser, $domain, $data, $captcha_solve);
                echo('Отправляю запрос...' . PHP_EOL);
                $browser->click('#admin-contact-submit');
//                $browser->screenshot("sending/{$domain->id}/sent");
                $browser->waitForText('Сообщение, отправленное вами, похоже на спам. Чтобы отправить письмо, перейдите по ссылке, отправленной на указанный e-mail отправителя.', 10);
                $browser->quit();
                $domain->update(['mail_account_id' => $account->id]);
            } catch (\Exception $exception) {
                $browser->screenshot("sending/error");
                if (!str_contains($browser->getOriginalBrowser()->driver->getCurrentURL(), 'reg.ru')) {
                    $domain->refresh()->setData('registrator', json_encode($browser->getOriginalBrowser()->driver->getCurrentURL()));
                    echo('domain:' . $browser->getOriginalBrowser()->driver->getCurrentURL() . PHP_EOL);
                    $domain->update([
                        'status_id' => Domain::STATUS_INVALID_REGISTRATOR,
                        'registrator' => $browser->getOriginalBrowser()->driver->getCurrentURL() ?? '',
                    ]);
                } else {
                    try {
                        echo('Ошибка: ' . $browser->text('.b-message_type_error .b-message__content') . PHP_EOL);
                    } catch (\Exception $exc) {
                    }
                }
                try {
                    $browser->quit();
                } catch (\Exception $ex) {
                }
                echo('Ошибка: ' . $exception->getMessage() . PHP_EOL);
                throw $exception;
            }
        });
    }

    /**
     * @param $browser
     * @param $domain
     * @param $data
     * @param $captcha_solve
     */
    private function fillInputs(&$browser, $domain, $data, $captcha_solve)
    {
        /** @var \Laravel\Dusk\Browser $browser */
        $browser->type('dname', $data['domain']);
        $browser->type('fio', $data['name']);
        $browser->type('email', $data['email']);
        $browser->type('message', $data['message']);
        $browser->script("$('#g-recaptcha-response').css({display: 'block'})");
        $browser->script("$('#admin-contact-submit').attr('disabled', false)");
        $browser->type('g-recaptcha-response', $captcha_solve);
    }


    private function getClient()
    {
        return new Client([
            'cookies' => true,
            'proxy' => 'http://om.mobileproxy.space:64017',
            'headers' => [
                'User-Agent' => Faker::create()->userAgent,
                'origin' => 'https://www.reg.ru',
                'referer' => 'https://www.reg.ru/',
            ],
        ]);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function reloadProxy()
    {
        while (true) {
            $resp = json_decode((new Client())->get('https://mobileproxy.space/reload.html?proxy_key=328c56147393e12fa2fe25903ba2804c&format=json', [
                'timeout' => 120,
                'verify' => false,
            ])->getBody()->getContents(), true);
            if ($resp['status'] == 'OK')
                break;
            else
                echo json_encode($resp);
        }
        echo('Прокси презагружен: ' . ($resp['new_ip']) . PHP_EOL);
    }
}
