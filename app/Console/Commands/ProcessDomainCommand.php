<?php

namespace App\Console\Commands;

use App\Jobs\ConfirmSendingJob;
use App\Models\Domain;
use App\Models\MailAccount;
use App\Models\Proxy;
use App\Services\MailService;
use App\Services\ProxyChrome;
use App\Services\RegruService;
use Faker\Factory as Faker;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use NunoMaduro\LaravelConsoleDusk\Manager;

class ProcessDomainCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'domain:process {account} {domain} {proxy?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $mail = new MailService($this);
        $account = MailAccount::find($this->argument('account'));
        $domain = Domain::find($this->argument('domain'));
        $proxy = $this->argument('proxy') == '' ? null : $this->argument('proxy');

//        for ($i = 0; $i < 20; $i++) {
//            try {
//                $this->reloadProxy();
        $this->sendMessage($mail, $account, $domain, $proxy);
//                break;
//            } catch (\Exception $exception) {
//                if ($i < 15) {
//                    echo('Ошибка: ' . $exception->getMessage());
//                    echo("Попытка " . ($i + 1) . "/15");
////                    $this->reloadProxy();
//                } else {
//                    throw $exception;
//                }
//            }
//        }
        return 0;
    }

    /**
     * @param \App\Services\MailService $mailService
     * @param \App\Models\MailAccount $account
     * @param \App\Models\Domain $domain
     * @param string $proxy
     * @throws \Facebook\WebDriver\Exception\TimeOutException
     */
    private function sendMessage(MailService $mailService, MailAccount $account, Domain $domain, $proxy)
    {
        $regruService = new RegruService($this);
        $this->info('Оправляю сообщение на домен: ' . $domain->domain . PHP_EOL);
        echo('Оправляю сообщение на домен: ' . $domain->domain . PHP_EOL);
        $faker = Faker::create();
//        $browser = new Manager(new ProxyChrome($proxy));
        $browser = null;

        $regruService->sendMessage($account, $domain, $browser, $this, [
            'name' => 'Юлия',
            'domain' => $domain->domain,
            'email' => $account->email,
            'message' => 'Добрый день.
Меня зовут Юлия. Я проанализировала примерную посещаемость вашего сайта ' . $domain->domain . ' через SimilarWeb и заинтересована в его покупке. Правда этот сервис не всегда точно показывает.

Поэтому готова предложить за ваш сайт цену из расчета: посещаемость ресурса по метрике за месяц умноженная на 2. Посчитайте, пожалуйста, стоимость вашего сайта и напишите, если рассматриваете продажу.

Для безопасности, сделку можем провести через гаранта или Телдери. Бюджет на покупку сайтов несколько миллионов рублей. Если ваш сайт дороже, то все равно смогу рассмотреть его покупку!

E-mail: Yulia.sites@yandex.ru
Telegram: https://t.me/YuliaSites

Спасибо. Жду вашего ответа.',
        ]);
        $domain->update(['status_id' => Domain::STATUS_NEED_VERIFICATION]);
        $this->info('Сообщение отправлено: ' . $domain->domain . PHP_EOL);
        echo('Сообщение отправлено: ' . $domain->domain . PHP_EOL);

        ConfirmSendingJob::dispatch($domain, $account)->onQueue('confirm')
            ->delay(now()->addMinutes(5));
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function reloadProxy()
    {
        while (true) {
            $resp = json_decode((new Client())->get('https://mobileproxy.space/reload.html?proxy_key=328c56147393e12fa2fe25903ba2804c&format=json', [
                'timeout' => 120,
            ])->getBody()->getContents(), true);
            if ($resp['status'] == 'OK')
                break;
        }
        echo('Прокси презагружен: ' . ($resp['new_ip']) . PHP_EOL);
    }
}
