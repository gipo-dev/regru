<?php

namespace App\Console\Commands;

use App\Models\Domain;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class CheckRegistratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'whois:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $pb = $this->output->createProgressBar(Domain::whereIn('status_id', [Domain::STATUS_VALID, Domain::STATUS_NEW])
            ->whereNull('registrator')->count());
        $pb->start();
//        Domain::whereIn('status_id', [Domain::STATUS_VALID, Domain::STATUS_NEW])
//            ->whereNull('registrator')->chunk(500, function ($domains) use ($pb) {
        while (true) {
            $domains = Domain::whereIn('status_id', [Domain::STATUS_VALID, Domain::STATUS_NEW])
                ->whereNull('registrator')->limit(500)->get();
            if (count($domains) < 1)
                break;
            foreach ($domains as $domain) {
                $process = new Process(['whois', $domain->domain]);
                $process->run(function ($e, $out) use ($domain) {
                    preg_match('/registrar\:\s*(.*)/', $out, $registrator);
                    $domain->update([
                        'registrator' => $registrator[1] ?? null,
                        'status_id' => ($registrator[1] ?? 'REGRU-RU') != 'REGRU-RU' ? Domain::STATUS_INVALID_REGISTRATOR : $domain->status_id,
                    ]);
                    $this->info($domain->domain . ': ' . ($registrator[1] ?? ''));
                });
                $pb->advance();
                usleep(600 * 1000);
            }
        }
//    });
        return 0;
    }
}
