<?php

namespace App\Orchid\Screens;

use App\Http\Requests\MailAccountRequest;
use App\Jobs\CreateMailUserJob;
use App\Models\MailAccount;
use Illuminate\Bus\Batch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class MailUserScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Почтовые ящики';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'mails' => MailAccount::latest('id')->paginate(100),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [

        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Group::make([
                    Input::make('count')->type('number')->value(1),
                    Button::make('Сгенерировать')->method('generate')
                        ->type(Color::SUCCESS())->icon('check'),
                ]),
            ]),

            Layout::table('mails', [
                TD::make('email', 'E-mail'),
                TD::make('password', 'Пароль'),
                TD::make('imap_password', 'Пароль IMAP'),
                TD::make('created_at', 'Дата создания')->render(function (MailAccount $account) {
                    return $account->created_at ? $account->created_at->format('d.m.Y H:i') : '';
                }),
                TD::make()
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (MailAccount $mailAccount) {
                        return Group::make([
                            Button::make('Удалить')->icon('trash')->type(Color::DANGER())
                                ->method('delete')
                                ->confirm('Удалить?')
                                ->parameters([
                                    'id' => $mailAccount->id,
                                ]),
                        ]);
                    }),
            ]),
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function delete(Request $request)
    {
        MailAccount::where('id', $request->id)->delete();
        Toast::success('Удалено');
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function generate(Request $request)
    {
        $jobs = [];

        for ($i = 0; $i < $request->get('count'); $i++)
            $jobs[] = new CreateMailUserJob();

        Bus::batch($jobs)->dispatch();
        Toast::success("Задача запущена");
    }
}
