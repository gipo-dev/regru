<?php

namespace App\Console\Commands;

use App\Models\Domain;
use Illuminate\Console\Command;

class ExportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'result:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->export(
            Domain::where('status_id', Domain::STATUS_VALID)
            , 'valid');
    }

    private function export($domains, $filename)
    {
        $rows = [];
        $domains->chunk(300, function ($list, $i) use (&$rows) {
            foreach ($list as $domain) {
                $rows[] = [
                    $domain->domain,
                    $domain->data['title'] ?? '',
                    $domain->data['description'] ?? '',
                    $domain->getInfo(),
                ];
            }
            $this->info($i);
        });
        $this->info('Saving...');
        $csv = fopen(storage_path('valid.csv'), 'wb');
        fputs($csv, chr(0xEF) . chr(0xBB) . chr(0xBF)); // BOM

        fputcsv($csv, ['Домен', 'Заголовок', 'Описание', 'Информация'], ';');

        collect($rows)->each(function (array $row) use ($csv) {
            fputcsv($csv, $row, ';');
        });

        fclose($csv);
    }
}
