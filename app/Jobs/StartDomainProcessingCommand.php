<?php

namespace App\Jobs;

use App\Models\Domain;
use App\Models\MailAccount;
use App\Models\Proxy;
use App\Services\BatchManager;
use App\Services\LoopManager;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StartDomainProcessingCommand implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    public $timeout = 20000;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $proxies = new LoopManager(array_merge([null], Proxy::all()->pluck('data')->toArray()));
        $accounts = new LoopManager(MailAccount::all()->all());
        $interval = 60 * 62 / (MailAccount::count() + 1);

        $domains = Domain::where('status_id', Domain::STATUS_VALID)->get(['id'])
            ->map(function (Domain $domain, $i) use ($proxies, $accounts, $interval) {
                return (new ProcessDomainJob($domain, $proxies->next(), $accounts->next(), $interval));
            })->toArray();
        (new BatchManager())->start($domains, BatchManager::TYPE_WEBSITE);
    }
}
