<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;

class KeyssoExporterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'keysso:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new Client([
            'headers' => [
                'X-Keyso-TOKEN' => '5e58f26d08d991.306579341a84b80201baf3fad69be764b9331536',
            ],
        ]);
        $rows = [];
        foreach (['msk', 'gru'] as $loc) {
            for ($i = 0; $i < 200; $i++) {
                $rows = array_merge($rows, json_decode(
                    $client->get('https://api.keys.so/report/simple/top_domain_visibility?base=' . $loc . '&domain=chevroletcars.ru&per_page=500&page=' . ($i + 1))
                        ->getBody()->getContents()
                    , true)['data']);
                $this->info($loc . ': ' . $i);
            }
            $this->export($rows, storage_path('domains_' . $loc . '.csv'));
        }
    }

    private function export($rows, $filepath)
    {
        $csv = fopen($filepath, 'wb');
        fputs($csv, chr(0xEF) . chr(0xBB) . chr(0xBF)); // BOM

        fputcsv($csv, ['ID', 'Домен', 'По видимости', 'в топ 1', 'в топ 3', 'в топ 5', 'в топ 10', 'в топ 50', 'Страниц в индексе', 'Объяв', 'Запросов в контексте', 'Трафик в контексте', 'Бюджет в контексте'], ';');

        collect($rows)->each(function (array $row) use ($csv) {
            fputcsv($csv, $row, ';');
        });

        fclose($csv);
    }
}
