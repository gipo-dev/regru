<?php

namespace App\Jobs;

use App\Models\Domain;
use App\Models\MailAccount;
use App\Models\Proxy;
use GuzzleHttp\Client;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;

class ProcessDomainJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Batchable;

    /**
     * @var int
     */
    public $timeout = 3600;

    /**
     * @var \App\Models\Domain
     */
    private $domain;

    /**
     * @var string
     */
    private $proxy;

    /**
     * @var \App\Models\MailAccount
     */
    private $account;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Domain $domain, $proxy, MailAccount $account, int $delay)
    {
        $this->domain = $domain;
        $this->proxy = $proxy;
        $this->account = $account;
        $this->delay = 240;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $startTime = microtime(true);
        try {
//            $this->reloadProxy();
            Artisan::call('domain:process', [
                'account' => $this->account->id,
                'domain' => $this->domain->id,
                'proxy' => $this->proxy ?? '',
            ]);
            $this->domain->setData('log', Artisan::output());
            $delay = $this->delay - round(microtime(true) - $startTime);
            if ($delay < 0)
                $delay = 0;
            sleep($delay);
        } catch (\Exception $exception) {
            $this->domain->setData('log', Artisan::output());
            $delay = $this->delay - round(microtime(true) - $startTime);
            if ($delay < 0)
                $delay = 0;
            sleep($delay);
            throw $exception;
        }
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function reloadProxy()
    {
        while (true) {
            $resp = json_decode((new Client())->get('https://mobileproxy.space/reload.html?proxy_key=328c56147393e12fa2fe25903ba2804c&format=json', [
                'timeout' => 120,
            ])->getBody()->getContents(), true);
            if ($resp['status'] == 'OK')
                break;
        }
        echo('Proxy: ' . ($resp['new_ip']) . PHP_EOL);
    }
}
