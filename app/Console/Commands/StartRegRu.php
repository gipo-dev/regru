<?php

namespace App\Console\Commands;

use App\Models\Domain;
use App\Models\MailAccount;
use App\Models\Proxy;
use App\Services\MailService;
use App\Services\ProxyChrome;
use App\Services\RegruService;
use Faker\Factory as Faker;
use Illuminate\Console\Command;
use NunoMaduro\LaravelConsoleDusk\Manager;

class StartRegRu extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'start:regru';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запускает рассылку по reg.ru';

    /**
     * @var \App\Services\RegruService
     */
    private $regRu;
    /**
     * @var array
     */
    private $proxyBrowsers;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->setupBrowsers();
        $mail = new MailService($this);
        $accounts = MailAccount::active()->get();

        foreach (Domain::where('status_id', Domain::STATUS_VALID)->get() as $domain) {
            foreach ($accounts as $account) {
                $this->sendMessage($mail, $account, $domain);
                sleep(60);
            }
        }
    }

    /**
     * @param \App\Services\MailService $mailService
     * @param \App\Models\MailAccount $account
     * @param \App\Models\Domain $domain
     */
    private function sendMessage(MailService $mailService, MailAccount $account, Domain $domain)
    {
        $regruService = new RegruService($this);
        $this->info('Оправляю сообщение на домен: ' . $domain->domain);
        $faker = Faker::create();

        $regruService->sendMessage($account, $domain, $this->getBrowser(), [
            'name' => $faker->firstNameMale,
            'domain' => $domain->domain,
            'email' => $account->email,
            'message' => 'Добрый день! Мне очень понравился ваш сайт. Рассматриваете вариант продажи?',
        ]);
        $domain->update(['status_id' => Domain::STATUS_NEED_VERIFICATION]);
        $this->info('Сообщение отправлено: ' . $domain->domain);

        $this->info('Подтверждаю отправку: ' . $domain->domain);
        $mailService->loginToMail($account->email, $account->imap_password);
        if ($mailService->confirmSending($domain)) {
            $domain->update(['status_id' => Domain::STATUS_SENT]);
            $this->info('Отправка подтверждена: ' . $domain->domain);
            $domain->setData('sent_time', now()->format('d-m-Y H:i'));
        } else {
            $this->info('Ошибка подтверждения: ' . $domain->domain);
            $domain->setData('sent_confirm_error', now()->format('d-m-Y H:i'));
        }
    }

    private $lastBrowser = -1;

    /**
     * @return mixed
     */
    private function getBrowser()
    {
        $this->lastBrowser++;
        if ($this->lastBrowser > (count($this->proxyBrowsers) - 1))
            $this->lastBrowser = 0;

        return $this->proxyBrowsers[$this->lastBrowser];
    }

    private function setupBrowsers()
    {
        $this->proxyBrowsers = [];

        $proxies = array_merge([
            null
        ], Proxy::all()->pluck('data')->toArray());

        foreach ($proxies as $proxy) {
            $this->proxyBrowsers[] = new Manager(new ProxyChrome($proxy));
        }
    }
}
