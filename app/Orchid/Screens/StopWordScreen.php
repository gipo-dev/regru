<?php

namespace App\Orchid\Screens;

use App\Models\Domain;
use App\Models\StopWord;
use App\Services\DomainChecker;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class StopWordScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Минус слова';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'stops' => StopWord::paginate(100),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Пересканировать')->type(Color::SUCCESS())
                ->icon('refresh')->method('rescan'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                TextArea::make('stops')->rows(5)
                    ->placeholder('Введите каждое слово с новой строки'),
                Button::make('Добавить слова')->method('generate')
                    ->type(Color::SUCCESS())->icon('check'),
            ]),

            Layout::table('stops', [
                TD::make('word', 'Слово'),
                TD::make()
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (StopWord $word) {
                        return Group::make([
                            Button::make('Удалить')->icon('trash')->type(Color::DANGER())
                                ->method('delete')
                                ->confirm('Удалить?')
                                ->parameters([
                                    'id' => $word->id,
                                ]),
                        ]);
                    }),
            ]),
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function delete(Request $request)
    {
        StopWord::where('id', $request->id)->delete();
        Toast::success('Удалено');
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function generate(Request $request)
    {
        $new = 0;
        foreach (array_filter(explode(PHP_EOL, $request->stops)) as $stop) {
            $word = StopWord::firstOrCreate(['word' => trim($stop)]);
            if ($word->wasRecentlyCreated)
                $new++;
        }
        Toast::success("Добавлено $new новых слов");
    }

    public function rescan(Request $request)
    {
        $checker = new DomainChecker();
        $result = [
            'valid' => 0,
            'invalid' => 0,
        ];
        foreach (Domain::whereIn('status_id', [Domain::STATUS_VALID, Domain::STATUS_INVALID])->get() as $domain) {
            /** @var Domain $domain */
            if ($checker->checkMinusWords($domain)) {
                if ($domain->status_id != Domain::STATUS_VALID) {
                    $domain->update(['status_id' => Domain::STATUS_VALID]);
                    $result['valid']++;
                }
            } else {
                if ($domain->status_id != Domain::STATUS_INVALID) {
                    $domain->update(['status_id' => Domain::STATUS_INVALID]);
                    $result['invalid']++;
                }
            }
        }
        Toast::success("Результат:\r\n Включено: {$result['valid']} " . PHP_EOL . " Отключено: {$result['invalid']}");
    }
}
