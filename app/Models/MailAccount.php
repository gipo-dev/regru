<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class MailAccount extends Model
{
    use HasFactory;
    use AsSource;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function scopeActive(Builder $builder)
    {
        $builder->whereNotNull('imap_password');
    }
}
