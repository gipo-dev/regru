<?php

namespace App\Console\Commands;

use App\Models\Domain;
use Illuminate\Console\Command;

class FillDomainsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fill:domains';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach ($this->getDomains() as $domain) {
            Domain::firstOrCreate([
                'domain' => $domain,
            ]);
        }
    }

    private function getDomains()
    {
        return collect(explode(PHP_EOL, file_get_contents(storage_path('domains.txt'))))->filter(function ($el) {
            return trim($el) != '';
        });
    }
}
