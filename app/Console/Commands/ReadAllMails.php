<?php

namespace App\Console\Commands;

use App\Models\MailAccount;
use App\Services\MailService;
use Illuminate\Console\Command;

class ReadAllMails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mails:read';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $service = new MailService($this);
        foreach (MailAccount::get() as $account) {
            $this->info($account->email);
            $service->readAllMessages($account->email, $account->imap_password);
        }
        return 0;
    }
}
