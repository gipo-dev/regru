<?php

namespace App\Jobs;

use App\Models\Domain;
use App\Services\DomainChecker;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;

class CheckDomainJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    public $timeout = 30;

    /**
     * @var \App\Models\Domain
     */
    private $domain;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Domain $domain)
    {
        $this->domain = $domain;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        Artisan::call('domain:check', ['domain_id' => $this->domain->id]);
        $checker = new DomainChecker();
        try {
            $checker->checkDomain($this->domain);
        } catch (\Exception $exception) {
            $this->domain->update(['status_id' => Domain::STATUS_INVALID]);
            $this->domain->setData('cms', $exception->getMessage());
        }
    }
}
