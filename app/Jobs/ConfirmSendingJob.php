<?php

namespace App\Jobs;

use App\Models\Domain;
use App\Models\MailAccount;
use App\Services\MailService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ConfirmSendingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var \App\Models\Domain
     */
    private $domain;

    /**
     * @var \App\Models\MailAccount
     */
    private $account;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Domain $domain, MailAccount $account)
    {
        $this->domain = $domain;
        $this->account = $account;
    }

    /**
     * @return string|null
     */
    public function getQueue(): ?string
    {
        return 'confirm';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $domain = $this->domain;
        $account = $this->account;
        echo('Подтверждаю отправку: ' . $domain->domain . PHP_EOL);
        for ($i = 0; $i < 20; $i++) {
            $mailService = new MailService($this);
            $mailService->loginToMail($account->email, $account->imap_password);
            if ($mailService->confirmSending($domain)) {
                $domain->update(['status_id' => Domain::STATUS_SENT]);
                $domain->refresh()->setData('sent_time', json_encode(now()->format('d-m-Y H:i')));
                echo('Отправка подтверждена: ' . $domain->domain . PHP_EOL);
                break;
            } else {
                echo('Ошибка подтверждения: ' . $domain->domain . PHP_EOL);
                $domain->setData('sent_confirm_error', now()->format('d-m-Y H:i'));
            }
        }
    }
}
