<?php

return [
    'domain' => [
        'status' => [
            \App\Models\Domain::STATUS_NEW => 'Ожидает обработки',
            \App\Models\Domain::STATUS_NEED_VERIFICATION => 'Ожидает подтверждения отправки письма',
            \App\Models\Domain::STATUS_SENT => 'Письмо отправлено',
            \App\Models\Domain::STATUS_VALID => 'Валиден',
            \App\Models\Domain::STATUS_INVALID => 'Не валиден',
            \App\Models\Domain::STATUS_INVALID_REGISTRATOR => 'Зарегистрирован не на рег.ру',
        ],

        'job' => [
            'status' => [
                1 => 'Остановлено',
                0 => 'Запущено',
            ],
        ],
    ],
];
