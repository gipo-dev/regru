<?php


namespace App\Services;


use Illuminate\Support\Collection;

class LoopManager
{
    /**
     * @var array
     */
    private $items;

    /**
     * @var int
     */
    private $lastIndex = -1;

    /**
     * LoopManager constructor.
     * @param array $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * @return mixed
     */
    public function next()
    {
        $this->lastIndex++;
        if ($this->lastIndex > (count($this->items) - 1))
            $this->lastIndex = 0;
        return $this->items[$this->lastIndex];
    }
}
