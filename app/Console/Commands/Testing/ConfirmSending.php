<?php

namespace App\Console\Commands\Testing;

use App\Models\Domain;
use App\Models\MailAccount;
use App\Services\MailService;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class ConfirmSending extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:sending:confirm {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $domain = Domain::find($this->argument('id'));
        $account = MailAccount::find($domain->mail_account_id);

        $mailService = new MailService($this);
        $mailService->loginToMail($account->email, $account->imap_password);
        $mailService->confirmSending($domain);
        return 0;
    }
}
